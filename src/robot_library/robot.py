#!/usr/bin/env python3

import rospy
import cv2
import numpy as np
from rosgraph_msgs.msg import Clock
from robot_library.srv import *
from rospy.names import resolve_name_without_node_name
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import time

class Robot:
    """Library class for working with pioneer-3dx in gazebo"""
    _pkg_name = "/robot_lib/"

    def __init__(self, waitTime=True):
        rospy.init_node("robot_library", anonymous=True)
        self.is_under_checker = bool(rospy.get_param("/is_under_checker"))
        if waitTime:
            while rospy.get_time() == 0.0:
                time.sleep(.01)

    def __get_clock_client(self):
        rospy.wait_for_service(self._pkg_name + 'get_clock')
        try:
            get_clock = rospy.ServiceProxy(self._pkg_name + 'get_clock', GetClock)
            result = get_clock(True)
            return result.clock
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)

    def __set_velosities_client(self, linear_vel, angular_vel):
        rospy.wait_for_service(self._pkg_name + 'set_velosities')
        try:
            set_velosities = rospy.ServiceProxy(self._pkg_name +  'set_velosities', SetVelosities)
            result = set_velosities(linear_vel, angular_vel)
            return result.isSuccess
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)
            return False


    def __get_laser_client(self):

        rospy.wait_for_service(self._pkg_name +  'get_laser')
        try:
            get_laser = rospy.ServiceProxy(self._pkg_name +  'get_laser', GetLaser)
            result = get_laser(True)
            return result.laser
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)

    def __get_distances_client(self):
        rospy.wait_for_service(self._pkg_name + 'get_distances')
        try:
            get_distances = rospy.ServiceProxy(self._pkg_name + "get_distances", GetDistances)
            result = get_distances()
            return result
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)

    def __get_direction_client(self):
        rospy.wait_for_service(self._pkg_name +  'get_direction')
        try:
            get_direction = rospy.ServiceProxy(self._pkg_name +  'get_direction', GetDirection)
            result = get_direction(True)
            return result.direction
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)

    def __get_camera_client(self):
        rospy.wait_for_service(self._pkg_name +  'get_camera')
        try:
            get_camera = rospy.ServiceProxy(self._pkg_name + 'get_camera', GetCamera)
            result = get_camera(True)
            return result.image
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)

    def __get_encoders_client(self):
        rospy.wait_for_service(self._pkg_name +  'get_encoders')
        try:
            get_encoders = rospy.ServiceProxy(self._pkg_name +  'get_encoders', GetEncoders)
            result = get_encoders(True)
            # print(result)
            return {'left': result.left, 'right': result.right}
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)



    def getDirection(self):
        """:return direction of robot"""
        return self.__get_direction_client()

    def getEncoders(self):
        """:return left and right encoders of the robot like dict"""
        return self.__get_encoders_client()

    def getLaser(self):
        """:returns laser values, time stamp for current values, angle of laser, 
        and angle increment for calculate values"""
        _laser = self.__get_laser_client()
        laser = {"time_stamp" : _laser.header.stamp.nsecs, "angle" : (_laser.angle_max * 2),
                 "angle_increment" : _laser.angle_increment, "values" : _laser.ranges}
        return laser

    def getDistances(self):
        """:return sensors values, time stamp for each"""
        _distances = self.__get_distances_client()
        if len(_distances.laser1.ranges) == 0:
            raise Exception("Method is not available for this robot")
        distances = {
            "front": {
                "timestamp": _distances.laser1.header.stamp.nsecs,
                "value": sum(_distances.laser1.ranges) / len(_distances.laser1.ranges)
            },
            "right": {
                "timestamp": _distances.laser2.header.stamp.nsecs,
                "value": sum(_distances.laser2.ranges) / len(_distances.laser2.ranges)
            },
            "back": {
                "timestamp": _distances.laser3.header.stamp.nsecs,
                "value": sum(_distances.laser3.ranges) / len(_distances.laser3.ranges)
            },
            "left": {
                "timestamp": _distances.laser4.header.stamp.nsecs,
                "value": sum(_distances.laser4.ranges) / len(_distances.laser4.ranges)
            },
        }
        return distances

    def setVelosities(self, linear_vel, angular_vel):
        """This function try to set up linear and angular velosities
        to the robot in gazebo simulation
        :return true if succseed"""
        return self.__set_velosities_client(linear_vel, angular_vel)

    def getImage(self):
        """:return camera image like openCV frame"""
        _frame = self.__get_camera_client()
        cv_image = CvBridge().imgmsg_to_cv2(_frame, desired_encoding="passthrough")
        return cv_image

    def time(self):
        """:return camera image like openCV frame"""
        #_clock = self.__get_clock_client()
        #if _clock is not None:
        #    return _clock.clock.secs + _clock.clock.nsecs / 1000000000
        #else:
        #    return 0.0
        return rospy.get_time()

    def sleep(self, sec):
        rospy.sleep(sec)
        #start_time = self.time()
        #while self.time() - start_time < sec:
        #    pass

    def notifyChecker(self, checkpoint_name, param=""):
        if self.is_under_checker:
            rospy.wait_for_service(self._pkg_name + 'notify_checker')
            notify_checker = rospy.ServiceProxy(self._pkg_name + 'notify_checker', NotifyChecker)
            notify_checker(checkpoint_name, param)