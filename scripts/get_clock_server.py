#!/usr/bin/env python3

import rospy
from robot_library.srv import *
from rosgraph_msgs.msg import Clock


clock = Clock()

def callback(msg):
    global clock
    clock = msg


def handle_get_clock(req):
    rospy.loginfo("New request for camera image")
    return GetClockResponse(clock)


def get_clock_server():
    rospy.init_node('get_clock_server',  anonymous=True)
    rospy.Subscriber("/clock", Clock, callback)
    s = rospy.Service('get_clock', GetClock, handle_get_clock)
    print("Ready to transmitting simulation clock information.")
    rospy.spin()

if __name__ == "__main__":
    get_clock_server()
