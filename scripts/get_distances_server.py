#!/usr/bin/env python3

import rospy
from robot_library.srv import *
from sensor_msgs import msg
from sensor_msgs.msg import LaserScan


if __name__ == "__main__":
    rospy.init_node("get_distances_server", anonymous=True)

    response = GetDistancesResponse()

    topics = ["front_us", "right_ir", "back_us", "left_ir"]

    def assign_msg(msg, idx):
        if idx == 0:
            response.laser1 = msg
        if idx == 1:
            response.laser2 = msg
        if idx == 2:
            response.laser3 = msg
        if idx == 3:
            response.laser4 = msg

    rospy.Subscriber(
        "/r1/front_us/scan",
        LaserScan,
        lambda msg: assign_msg(msg, 0))

    rospy.Subscriber(
        "/r1/right_ir/scan",
        LaserScan,
        lambda msg: assign_msg(msg, 1))
    
    rospy.Subscriber(
        "/r1/back_us/scan",
        LaserScan,
        lambda msg: assign_msg(msg, 2))

    rospy.Subscriber(
        "/r1/left_ir/scan",
        LaserScan,
        lambda msg: assign_msg(msg, 3))

    rospy.Service(
        "get_distances",
        GetDistances,
        lambda req:
            response)

    rospy.spin()
